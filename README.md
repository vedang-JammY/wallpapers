# My Wallpaper Collection

Wallpapers I have collected throughout my linux journey.

## Where Did I Get These?

I find wallpapers in distrotube's gitlab repo. here's the sauce [Derek Taylor](https://gitlab.com/dwt1/wallpapers). 
I added more wallpapers of my own to make this wholesome collection. 
I am updating it on regular basis.
